package com.tac.media.audioplayer.interfaces;

/**
 * Created by dima on 9/19/14.
 */
public interface IRecordUpdate {

    abstract void byteRecord(float data);

    abstract void updateTime(long millisecond);
}
